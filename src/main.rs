extern crate mac_notification_sys;
extern crate rand;
use mac_notification_sys::*;
use rand::prelude::*;
use std::{thread, time};

fn main() {
    let motivational_quotes = vec![
        "Now is better than later",
        "One now is better than two later",
        "Fail fast",
        "You can do it",
        "you're a great person",
    ];

    // thread_rng is often the most convenient source of randomness:
    let mut rng = thread_rng();

    let bundle = get_bundle_identifier_or_default("Xcode");
    set_application(&bundle).unwrap();
    loop {
        let motivational_string_to_display =
            motivational_quotes[rng.gen_range(0, motivational_quotes.len())];
        println!("{}", motivational_string_to_display);
        send_notification(
            &format!("{}", motivational_string_to_display),
            &None,
            "",
            &None,
        ).unwrap();
        let sleep_duration = time::Duration::from_secs(1700);
        thread::sleep(sleep_duration);
    }
}
